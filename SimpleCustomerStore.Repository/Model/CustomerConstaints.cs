namespace SimpleCustomerStore.Repository.Model {
    public static class CustomerConstaints {
        public const int MinNameLength = 2;
        public const int MaxNameLength = 100;
        public const int MinSurnameLength = 2;
        public const int MaxSurnameLength = 100;
        public const int MinTelephoneLength = 5;
        public const int MaxTelephoneLength = 20;
        public const int MinAdressLength = 2;
        public const int MaxAdressLength = 1000;
    }
}