﻿namespace SimpleCustomerStore.Repository.Model {
    public interface IDeepClonable<out T> {
        T DeepClone();
    }
}
