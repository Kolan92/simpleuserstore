﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SimpleCustomerStore.Repository.Model {
    public class Customer : IDeepClonable<Customer>, IEquatable<Customer> {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(CustomerConstaints.MinNameLength)]
        [MaxLength(CustomerConstaints.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [MinLength(CustomerConstaints.MinSurnameLength)]
        [MaxLength(CustomerConstaints.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [MinLength(CustomerConstaints.MinTelephoneLength)]
        [MaxLength(CustomerConstaints.MaxTelephoneLength)]
        public string TelephoneNumber { get; set; }

        [Required]
        [MinLength(CustomerConstaints.MinAdressLength)]
        [MaxLength(CustomerConstaints.MaxAdressLength)]
        public string Address { get; set; }

        public Customer DeepClone() {
            var serialized = JsonConvert.SerializeObject(this);
            return JsonConvert.DeserializeObject<Customer>(serialized);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Customer) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Id;
                hashCode = (hashCode * 397) ^ (Name?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (Surname?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (TelephoneNumber?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ (Address?.GetHashCode() ?? 0);
                return hashCode;
            }
        }

        public bool Equals(Customer other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && string.Equals(Name, other.Name) && string.Equals(Surname, other.Surname) && string.Equals(TelephoneNumber, other.TelephoneNumber) && string.Equals(Address, other.Address);
        }
    }
}
