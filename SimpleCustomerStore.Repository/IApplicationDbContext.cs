﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using SimpleCustomerStore.Repository.Model;

namespace SimpleCustomerStore.Repository {
    public interface IApplicationDbContext {
        DbSet<Customer> Customers { get; set; }
        DbEntityEntry Entry(object entity);

        int SaveChanges();
    }
}
