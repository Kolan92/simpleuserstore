﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using SimpleCustomerStore.Repository.Model;

namespace SimpleCustomerStore.Repository.Repository {
    public class CustomerRepository : ICustomerRepository {
        private readonly IApplicationDbContext _dbDbContext;
        public CustomerRepository(DbConnection connection) {
            _dbDbContext = new ApplicationDbContext(connection);
        }

        public CustomerRepository(IApplicationDbContext dbContext) {
            _dbDbContext = dbContext;
        }
        public IQueryable<Customer> GetAll() {
            return _dbDbContext.Customers.AsQueryable();
        }

        public Customer GetById(int id) {
            return _dbDbContext.Customers.SingleOrDefault(c => c.Id == id);
        }

        public void Insert(params Customer[] customers) {
            if (customers == null)
                throw new ArgumentNullException(nameof(customers), $"{nameof(customers)} cannot be null");
            if (customers.Any(c => c == null))
                throw new ArgumentNullException(nameof(customers), $"{nameof(customers)} cannot contain null");

            _dbDbContext.Customers.AddRange(customers);
            _dbDbContext.SaveChanges();
        }

        public void Update(Customer customer) {
            if(customer == null)
                throw new ArgumentNullException(nameof(customer), $"{nameof(customer)} cannot be null");

            var exsitingCustomer = GetById(customer.Id);
            if (exsitingCustomer == null)
                throw new InvalidOperationException($"{nameof(Customer)} with id:{customer.Id} was not found");

            _dbDbContext.Entry(exsitingCustomer).CurrentValues.SetValues(customer);
            _dbDbContext.SaveChanges();
        }

        public void Remove(int id) {
            var cutomerToRemove = GetById(id);
            if (cutomerToRemove == null)
                throw new InvalidOperationException($"{nameof(Customer)} with id:{id} was not found");
            _dbDbContext.Entry(cutomerToRemove).State = EntityState.Deleted;
            _dbDbContext.SaveChanges();
        }
    }
}
