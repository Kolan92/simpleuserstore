﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleCustomerStore.Repository.Model;

namespace SimpleCustomerStore.Repository.Repository {
    public interface ICustomerRepository {
        IQueryable<Customer> GetAll();
        Customer GetById(int id);
        void Insert(params Customer[] customers);
        void Update(Customer customer);
        void Remove(int id);
    }
}
