﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleCustomerStore.Repository.Model;

namespace SimpleCustomerStore.Repository {
    public class ApplicationDbContext : DbContext, IApplicationDbContext {
        public DbSet<Customer> Customers { get; set; }

        public ApplicationDbContext()
          : base("name=SimpleCustomerStore") {

        }

        public ApplicationDbContext(DbConnection connection)
            : base(connection, true) {
        }
    }
}
