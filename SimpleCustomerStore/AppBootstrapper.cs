﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses;
using Nancy.Session;
using Nancy.TinyIoc;
using SimpleCustomerStore.Repository;
using SimpleCustomerStore.Repository.Repository;

namespace SimpleCustomerStore {
    public class AppBootstrapper : DefaultNancyBootstrapper {
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines) {
            CookieBasedSessions.Enable(pipelines);
        }

        protected override void ConfigureApplicationContainer(TinyIoCContainer container) {
            base.ConfigureApplicationContainer(container);

            container.Register<IApplicationDbContext, ApplicationDbContext>();
            container.Register<ICustomerRepository, CustomerRepository>()
                .UsingConstructor(() => new CustomerRepository(new ApplicationDbContext()));
        }
    }
}
