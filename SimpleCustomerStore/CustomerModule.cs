﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Nancy;
using Nancy.Extensions;
using Nancy.ModelBinding;
using Nancy.Validation;
using Newtonsoft.Json;
using SimpleCustomerStore.Repository.Model;
using SimpleCustomerStore.Repository.Repository;
using SimpleCustomerStore.RequestModels;

namespace SimpleCustomerStore {
    public class CustomerModule: NancyModule {
        public CustomerModule(ICustomerRepository customerRepository)
            :base("customers") {
            var customerRepository1 = customerRepository;

            Get["/"] = _ => {
                try {
                    var customers = customerRepository1.GetAll();
                    return Response.AsJson(customers).WithStatusCode(HttpStatusCode.OK);
                }
                catch (Exception exception) {
                    return CreateExceptionResponse(exception);
                }
            };

            Delete["/{id:int}"] = parmeters => {
                try {
                    customerRepository1.Remove(parmeters.id);
                    return HttpStatusCode.OK;
                }
                catch (Exception exception) {
                    return CreateExceptionResponse(exception);
                }
            };

            Post["/"] = parameters => {
                try {
                    var customerInfo = this.Bind<CustomerRequest>();
                    var validationResult = this.Validate(customerInfo);

                    if (!validationResult.IsValid) {
                        return CreateValidationErrorResponse(validationResult);
                    }

                    var customer = new Customer {
                        Name = customerInfo.Name,
                        Surname = customerInfo.Surname,
                        TelephoneNumber = customerInfo.Telephone,
                        Address = customerInfo.Adress
                    };
                    customerRepository1.Insert(customer);
                    return HttpStatusCode.OK;
                }
                catch (Exception exception) {
                    return CreateExceptionResponse(exception);
                }
            };

            Put["/{id:int}"] = parameters => {
                try {
                    var customerInfo = this.Bind<CustomerRequest>();
                    var validationResult = this.Validate(customerInfo);

                    if (!validationResult.IsValid) {
                        return CreateValidationErrorResponse(validationResult);
                    }

                    var customer = new Customer {
                        Id = parameters.id,
                        Name = customerInfo.Name,
                        Surname = customerInfo.Surname,
                        TelephoneNumber = customerInfo.Telephone,
                        Address = customerInfo.Adress
                    };

                    customerRepository1.Update(customer);
                    return HttpStatusCode.OK;
                }
                catch (Exception exception) {
                    return CreateExceptionResponse(exception);
                }
            };
        }

        private Response CreateValidationErrorResponse(ModelValidationResult validationResult) {
            var errorMessage = string.Join(" ",
                        validationResult.Errors.SelectMany(x => x.Value.Select(y => y.ErrorMessage)));
            return Response.AsText(errorMessage).WithStatusCode(HttpStatusCode.BadRequest);
        }

        private Response CreateExceptionResponse(Exception exception) {
            return Response.AsText(exception.Message).WithStatusCode(HttpStatusCode.BadRequest);
        }
    }
}
