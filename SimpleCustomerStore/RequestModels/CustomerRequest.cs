﻿namespace SimpleCustomerStore.RequestModels {
    public class CustomerRequest {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Telephone { get; set; }
        public string  Adress { get; set; }
    }
}
