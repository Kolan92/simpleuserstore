﻿using FluentValidation;
using Customer = SimpleCustomerStore.Repository.Model.CustomerConstaints;
using SimpleCustomerStore.RequestModels;

namespace SimpleCustomerStore.RequestValidator {
    public class CustomerRequestValidator : AbstractValidator<CustomerRequest> {

        public CustomerRequestValidator() {
            RuleFor(request => request.Name)
                .NotNull().
                WithMessage("You must specify a customer name.");
            RuleFor(request => request.Name)
                .Length(Customer.MinNameLength, Customer.MaxNameLength)
                .WithMessage($"The customer name must be between {Customer.MinNameLength} and {Customer.MaxNameLength} characters long.");

            RuleFor(request => request.Surname)
                .NotNull()
                .WithMessage("You must specify a customer surname.");
            RuleFor(request => request.Surname)
                .Length(Customer.MinSurnameLength, Customer.MaxSurnameLength)
                .WithMessage($"The customer surname must be between {Customer.MinSurnameLength} and {Customer.MaxSurnameLength} characters long.");

            RuleFor(request => request.Telephone)
                .NotNull()
                .WithMessage("You must specify a customer telephone.");
            RuleFor(request => request.Telephone)
                .Length(Customer.MinTelephoneLength, Customer.MaxTelephoneLength)
                .WithMessage($"The customer telephone must be between {Customer.MinTelephoneLength} and {Customer.MaxTelephoneLength} characters long.");
            RuleFor(request => request.Telephone)
                .Matches("^[0-9]*$")
                .WithMessage("The customer telephone is in valid format");

            RuleFor(request => request.Adress)
                .NotNull()
                .WithMessage("You must specify a customer adress.");
            RuleFor(request => request.Adress)
                .Length(Customer.MinAdressLength, Customer.MaxAdressLength)
                .WithMessage($"The customer adress must be between {Customer.MinAdressLength} and {Customer.MaxAdressLength} characters long.");
        }
    }
}
