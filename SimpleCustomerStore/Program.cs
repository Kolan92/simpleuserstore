﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy.Hosting.Self;

namespace SimpleCustomerStore {
    class Program {
        private const string Url = "http://localhost";
        private const int Port = 12345;
        private readonly NancyHost _nancy;

        public Program() {
            var configuration = new HostConfiguration {
                UrlReservations = new UrlReservations { CreateAutomatically = true }
            };
            var uri = new Uri($"{Url}:{Port}/");
            _nancy = new NancyHost(configuration, uri);
        }

        private void Start() {
            _nancy.Start();
            Console.WriteLine($"Started listennig port {Port}");
            Console.ReadKey();
            _nancy.Stop();
        }

        static void Main(string[] args) {
            var program = new Program();
            program.Start();
        }
    }
}
