﻿using System.Globalization;
using System.Threading;
using FluentAssertions;
using Nancy.Testing;
using Newtonsoft.Json;
using NUnit.Framework;
using SimpleCustomerStore;
using SimpleCustomerStore.Repository.Model;
using SimpleCustomerStore.Repository.Repository;
using SimpleCustomerStore.RequestModels;
using Browser = Nancy.Testing.Browser;
using HttpStatusCode = Nancy.HttpStatusCode;

namespace SimpleCustormerStore.Test {
    [TestFixture]
    public class ApiTests {
        private Browser _browser;
        private ICustomerRepository _customerRepository;
        private readonly Customer[] customerData = new[] {
            new Customer {
                Name = "Jan",
                Surname = "Kowalski",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            },
            new Customer {
                Name = "Andrzej",
                Surname = "Nowak",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            },
            new Customer {
                Name = "Anna",
                Surname = "Testowa",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            }
        };

        [SetUp]
        public void SetUp() {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

            var connection = Effort.DbConnectionFactory.CreateTransient();
            _customerRepository = new CustomerRepository(connection);
            _customerRepository.Insert(customerData);

            _browser = new Browser( config => {
                config.Module<CustomerModule>();
                config.Dependency<ICustomerRepository>(_customerRepository);
            });
        }

        [Test]
        public void Should_Return_Staus_Ok_When_Route_Exists_And_Body_Is_Json() {
            var response = _browser.Get("/customers", with => {
                with.HttpRequest();
            });

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.ContentType.Should().Be("application/json; charset=utf-8");
        }

        [Test]
        public void Should_Return_Stored_Customer_Collection() {
            var response = _browser.Get("/customers", with => {
                with.HttpRequest();
            });

            response.Body.AsString().Should().BeEquivalentTo(JsonConvert.SerializeObject(customerData));
        }

        [Test]
        public void Should_Return_Bad_Request_When_Delete_Non_Existing_Customer() {
            var nonExsitingId = 42;
            var response = _browser.Delete($"/customers/{nonExsitingId}", with => {
                with.HttpRequest();
            });

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Body.AsString().Should().Be($"Customer with id:{nonExsitingId} was not found");
        }

        [Test]
        public void Shoud_Delete_Customer_From_Database() {
            var response = _browser.Delete("/customers/1", with => {
                with.HttpRequest();
            });

            var customerCollection = _customerRepository.GetAll();

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            customerCollection.Should().HaveCount(2);
        }

        [Test]
        public void Shoud_Not_Insert_Not_Valid_Customer_To_Database() {
            var notValidAddCustomerRequest = new CustomerRequest
            {
                Name = "A",
                Surname = "Kowalski",
                Telephone = "1234567899",
                Adress = "Testing street"
            };

            var response = _browser.Post("/customers", with => {
                with.Body(JsonConvert.SerializeObject(notValidAddCustomerRequest));
                with.Header("Content-Type", "application/json");
                with.HttpRequest();
            });

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Body.AsString().Should().Be("The customer name must be between 2 and 100 characters long.");
        }

        [Test]
        public void Should_Insert_Valid_Customer_To_Database() {
            var validAddCustomerRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = "Kowalski",
                Telephone = "1234567899",
                Adress = "Testing street"
            };

            var response = _browser.Post("/customers", with => {
                with.Body(JsonConvert.SerializeObject(validAddCustomerRequest));
                with.Header("Content-Type", "application/json");
                with.HttpRequest();
            });
            var customers = _customerRepository.GetAll();

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            customers.Should().HaveCount(customerData.Length + 1);
        }

        [Test]
        public void Shoud_Not_Update_Not_Valid_Customer_To_Database() {
            var exsistingId = 42;
            var nonValidAddCustomerRequest = new CustomerRequest {
                Name = "A",
                Surname = "Kowalski",
                Telephone = "1234567899",
                Adress = "Testing street"
            };

            var response = _browser.Put($"/customers/{exsistingId}", with => {
                with.Body(JsonConvert.SerializeObject(nonValidAddCustomerRequest));
                with.Header("Content-Type", "application/json");
                with.HttpRequest();
            });

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Body.AsString().Should().Be("The customer name must be between 2 and 100 characters long.");
        }

        [Test]
        public void Should_Not_Update_NonExisting_Customer() {
            var nonExsistingId = 42;
            var validUpdateCustomerRequest = new CustomerRequest {
                Name = "Jan",
                Surname = "Kowalski",
                Telephone = "1234567899",
                Adress = "Testing street"
            };

            var response = _browser.Put($"/customers/{nonExsistingId}", with => {
                with.Body(JsonConvert.SerializeObject(validUpdateCustomerRequest));
                with.Header("Content-Type", "application/json");
                with.HttpRequest();
            });

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Body.AsString().Should().Be($"{nameof(Customer)} with id:{nonExsistingId} was not found");
        }

        [Test]
        public void Should_Update_Existing_Customer() {
            var exsistingId = 1;
            var validUpdateCustomerRequest = new CustomerRequest
            {
                Name = "Nie Jan",
                Surname = "Kowalski",
                Telephone = "1234567899",
                Adress = "Testing street"
            };

            var response = _browser.Put($"/customers/{exsistingId}", with => {
                with.Body(JsonConvert.SerializeObject(validUpdateCustomerRequest));
                with.Header("Content-Type", "application/json");
                with.HttpRequest();
            });

            var updatedCustomer = _customerRepository.GetById(exsistingId);
            updatedCustomer.Name.Should().Be(validUpdateCustomerRequest.Name);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}
