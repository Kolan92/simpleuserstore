﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using SimpleCustomerStore.RequestModels;
using SimpleCustomerStore.RequestValidator;

namespace SimpleCustormerStore.Test {
    [TestFixture]
    public class AddUserValidatorTests {
        private CustomerRequestValidator _requestValidator;

        [SetUp]
        public void SetUp() {
            _requestValidator = new CustomerRequestValidator();;
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Too_Short_Name() {
            var customerAddRequest = new CustomerRequest {
                Name = "A",
                Surname = "Kowalski",
                Telephone = "555 444 123",
                Adress = "Testing street"
            };
            
            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Too_Long_Name() {
            var customerAddRequest = new CustomerRequest
            {
                Name = new string('a', 101),
                Surname = "Kowalski",
                Telephone = "555 444 123",
                Adress = "Testing street"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Null_Name() {
            var customerAddRequest = new CustomerRequest
            {
                Name = null,
                Surname = "Kowalski",
                Telephone = "555 444 123",
                Adress = "Testing street"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Too_Short_Surame() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = "K",
                Telephone = "555 444 123",
                Adress = "Testing street"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Too_Long_Surname() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = new string('a', 101),
                Telephone = "555 444 123",
                Adress = "Testing street"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Null_Surname() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = null,
                Telephone = "555 444 123",
                Adress = "Testing street"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Too_Short_Telephone() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = "Kowalski",
                Telephone = "555",
                Adress = "Testing street"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Too_Long_Telephone() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = "Kowalski",
                Telephone = new string('2', 21),
                Adress = "Testing street"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Null_Telephone() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = "Kowalski",
                Telephone = null,
                Adress = "Testing street"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Valid_Given_Telephone_Number() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = "Kowalski",
                Telephone = "1234567899",
                Adress = "Testing street"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);

            validationResult.IsValid.Should().BeTrue();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Telephone_With_Alphabet_Characters() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = "Kowalski",
                Telephone = "tel number",
                Adress = "Testing street"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Too_Short_Adress() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = "Kowalski",
                Telephone = "555 444 123",
                Adress = "A"
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Too_Long_Adress() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = "Kowalski",
                Telephone = "555 444 123",
                Adress = new string('a', 1001)
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }

        [Test]
        public void Should_Return_Not_Valid_Given_Null_Adress() {
            var customerAddRequest = new CustomerRequest
            {
                Name = "Jan",
                Surname = "Kowalski",
                Telephone = "555 444 123",
                Adress = null
            };

            var validationResult = _requestValidator.Validate(customerAddRequest);
            validationResult.IsValid.Should().BeFalse();
        }
    }
}
