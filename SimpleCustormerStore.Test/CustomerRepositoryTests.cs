﻿using FluentAssertions;
using NUnit.Framework;
using SimpleCustomerStore.Repository.Model;
using SimpleCustomerStore.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

namespace SimpleCustormerStore.Test {
    [TestFixture]
    public class CustomerRepositoryTests {
        private ICustomerRepository _customerRepository;

        [SetUp]
        public void SetUp() {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

            var connection = Effort.DbConnectionFactory.CreateTransient();
            _customerRepository = new CustomerRepository(connection);
        }

        [Test]
        public void Should_Return_Empty_Collection_Of_Customer_When_Database_Is_Empty() {
            var customersCollection = _customerRepository.GetAll();
            customersCollection.Should().HaveCount(0);
        }

        [Test]
        public void Should_Return_Null_When_Geting_Customer_By_Non_Existing_Id() {
            var nonExistingId = 42;
            var customer = _customerRepository.GetById(nonExistingId);
            customer.Should().BeNull();
        }

        [Test]
        public void Should_Throw_Exception_When_Insert_Null() {
            _customerRepository.Invoking(it => it.Insert(null))
               .ShouldThrow<ArgumentNullException>()
               .WithMessage("customers cannot be null\r\nParameter name: customers");
        }

        [Test]
        public void Should_Throw_Exception_When_Insert_Collection_With_Null() {
            var customerCollection = new[] {
                new Customer {
                    Name = "Test",
                    Surname = "Test",
                    TelephoneNumber = "654654654",
                    Address = "Testing street"
                },
                null
            };

            _customerRepository.Invoking(it => it.Insert(customerCollection))
              .ShouldThrow<ArgumentNullException>()
              .WithMessage("customers cannot contain null\r\nParameter name: customers");
        }

        [Test]
        public void Should_Add_Customer_To_Database() {
            var customer = new Customer {
                Name = "Test",
                Surname = "Test",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            };
            _customerRepository.Insert(customer);

            var customerCollection = _customerRepository.GetAll();
            customerCollection.Should().HaveCount(1);
        }

        [Test]
        public void Should_Add_Customer_Collection_To_Database() {
            var customerCollection = new[] {
                new Customer {
                    Name = "Test1",
                    Surname = "Test1",
                    TelephoneNumber = "654654654",
                    Address = "Testing street"
                },
                new Customer {
                    Name = "Test2",
                    Surname = "Test2",
                    TelephoneNumber = "654654654",
                    Address = "Testing street"
                },
                new Customer {
                    Name = "Test3",
                    Surname = "Test3",
                    TelephoneNumber = "654654654",
                    Address = "Testing street"
                }
            };

            _customerRepository.Insert(customerCollection);

            var storedCustomerCollection = _customerRepository.GetAll();
            storedCustomerCollection.Should().HaveCount(3);
        }

        [Test]
        public void Should_First_Inserted_Customer_Have_Id_1() {
            var customer = new Customer
            {
                Name = "Test",
                Surname = "Test",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            };
            _customerRepository.Insert(customer);

            var foundUser = _customerRepository.GetById(1);
            foundUser.Should().BeOfType<Customer>()
                .Which.Id.Should().Be(1);
        }

        [Test]
        public void Should_Throw_Exception_When_Try_Delete_Non_Existing_Customer() {
            var nonExistingId = 42;
            _customerRepository.Invoking(it => it.Remove(nonExistingId))
                .ShouldThrow<InvalidOperationException>()
                .WithMessage($"{nameof(Customer)} with id:{nonExistingId} was not found");
        }

        [Test]
        public void Should_Delete_Customer_Entity_From_Database() {
            var customer = new Customer
            {
                Name = "Test",
                Surname = "Test",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            };
            _customerRepository.Insert(customer);

            _customerRepository.Remove(id:1);
            var customerCollection = _customerRepository.GetAll();
            customerCollection.Should().HaveCount(0);
        }

        [Test]
        public void Should_Throw_Exception_When_Update_Null_Object() {
            _customerRepository.Invoking(it => it.Update(null))
               .ShouldThrow<ArgumentNullException>()
               .WithMessage("customer cannot be null\r\nParameter name: customer");
        }

        [Test]
        public void Should_Throw_Exception_When_Update_Non_Existing_Customer() {
            var nonExistingId = 42;
            var customer = new Customer
            {
                Id = nonExistingId,
                Name = "Test",
                Surname = "Test",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            };
            _customerRepository.Invoking(it => it.Update(customer))
                .ShouldThrow<InvalidOperationException>()
                .WithMessage($"{nameof(Customer)} with id:{nonExistingId} was not found");
        }

        [Test]
        public void Should_Change_Customer_Properties() {
            var customer = new Customer
            {
                Name = "Test",
                Surname = "Test",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            };
            _customerRepository.Insert(customer);

            var changedName = "changedName";
            var changedSurname = "changedSurname";

            var customerToChange = _customerRepository.GetById(1).DeepClone(); //deep copy to separate this object from context
            customerToChange.Name = changedName;
            customerToChange.Surname = changedSurname;
            _customerRepository.Update(customerToChange);

            var changedCustomer = _customerRepository.GetById(1);
            changedCustomer.Should().Be(customerToChange);
        }
    }
}
