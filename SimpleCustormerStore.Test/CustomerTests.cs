﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using SimpleCustomerStore.Repository.Model;

namespace SimpleCustormerStore.Test {
    [TestFixture]
    public class CustomerTests {
        [Test]
        public void Should_Customer_And_Null_Not_Be_Equal() {
            var customer = new Customer
            {
                Name = "Test",
                Surname = "Test",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            };

            var isEqual = customer.Equals(null);
            isEqual.Should().BeFalse();
        }

        [Test]
        public void Should_Two_Customers_With_All_Equl_Properties_Be_Equal() {
            var customerA = new Customer
            {
                Name = "Test",
                Surname = "Test",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            };
            var customerB = new Customer
            {
                Name = "Test",
                Surname = "Test",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            };

            var isEqual = customerA.Equals(customerB);
            isEqual.Should().BeTrue();
        }

        [Test]
        public void Should_DeepClone_Return_Equal_Object_But_Not_Same() {
            var orginal = new Customer
            {
                Name = "Test",
                Surname = "Test",
                TelephoneNumber = "654654654",
                Address = "Testing street"
            };

            var cloned = orginal.DeepClone();
            cloned.Should().NotBeSameAs(orginal).And.Be(orginal);
        }
    }
}
